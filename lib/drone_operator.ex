defmodule DroneOperator do
  @moduledoc false

  use GenServer
  require Logger

  def start_link(args) do
    GenServer.start_link(__MODULE__, args)
  end

  @impl GenServer
  def init(args) do
    buttons = Keyword.get(args, :buttons)
    leds = Keyword.get(args, :leds)

    button_mappings =
      buttons
      |> Enum.map(
          fn {chip, pin, description, response} ->
            :ok = Circuits.GPIO.Chip.listen_event(chip, pin)
            {Integer.to_string(pin), description, response} end)
      |> Map.new(fn {pin, description, response} -> {pin, {description, response}} end)


    led_mappings =
      leds
      |>  Enum.map(
        fn {chip, pin, identity} ->
          {:ok, line} = Circuits.GPIO.Chip.request_line(chip, pin, :output)
          :ok = Circuits.GPIO.Chip.set_value(line, 1)
          {identity, line}
        end
      )
      |> Map.new(fn {identity, line} -> {identity, line} end)

    {:ok, {button_mappings, led_mappings}}
  end

  @impl GenServer
  def handle_info({:circuits_cdev, pin, _timestamp, 1}, {button_mappings, led_mappings}) do
    {description, _response} = Map.fetch!(button_mappings, Integer.to_string(pin))
    Logger.info("'#{description}' button released")
    #TODO: Send response event to pub-sub
    {:noreply, {button_mappings, led_mappings}}
  end

  def handle_info({:circuits_cdev, pin, _timestamp, 0}, {button_mappings, led_mappings}) do
    {description, response} = Map.fetch!(button_mappings, Integer.to_string(pin))
    Logger.debug("'#{description}' button pressed")
    DroneAction.action(response)
    {:noreply, {button_mappings, led_mappings}}
  end
end
