defmodule DroneOperator.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  defp get_drone_operator_worker() do
    drone_args =
      [
        {
          :buttons,
          [
            {"gpiochip0", 130, "Shutdown", :shutdown_button},
            {"gpiochip0", 133, "Gear-up", :gear_up_button},
            {"gpiochip0", 131, "Gear-down", :gear_down_button}
          ]
        },
        {
          :leds,
          [
            {"gpiochip0", 136, :running_led}
          ]
        }
      ]

    case Application.get_env(:drone_operator, :hardware) do
      :nohardware -> []
      _ -> [{DroneOperator, drone_args}]
    end
  end

  @impl true
  def start(_type, _args) do

    children = [
      # Starts a worker by calling: DroneOperator.Worker.start_link(arg)
      {DroneAction, []},

      # Start the Telemetry supervisor
      DroneOperatorWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: DroneOperator.PubSub},
      # Start the Endpoint (http/https)
      DroneOperatorWeb.Endpoint
    ] ++ get_drone_operator_worker()

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: DroneOperator.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    DroneOperatorWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
