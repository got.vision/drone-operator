defmodule DroneOperatorWeb.PageController do
  use DroneOperatorWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
