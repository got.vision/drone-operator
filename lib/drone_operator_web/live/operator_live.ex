defmodule DroneOperatorWeb.OperatorLive do
  use Phoenix.LiveView

  require Logger

  def mount(_session, socket) do
    {:ok, socket}
  end

  def handle_event("landing_gear_up", _, socket) do
    Logger.info("Clicked landing gear up")
    DroneAction.action(:gear_up_button)
    {:noreply, socket}
  end

  def handle_event("landing_gear_down", _, socket) do
    Logger.info("Clicked landing gear down")
    DroneAction.action(:gear_down_button)
    {:noreply, socket}
  end

  def handle_event("shutdown_os", _, socket) do
    Logger.info("Clicked shutdown OS")
    DroneAction.action(:shutdown_button)
    {:noreply, socket}
  end

  def render(assigns) do
    ~L"""
    <div id="liveoperator_landinggear_container">
      <h1>Operate langing gear:</h1>
      <button phx-click="landing_gear_up">UP</button>
      <button phx-click="landing_gear_down">DOWN</button>
    </div>
    <div id="liveoperator_shutdown_container">
      <h1>Shut down clean:</h1>
      <button phx-click="shutdown_os">Shutdown OS</button>
    </div>
    """
  end
end
