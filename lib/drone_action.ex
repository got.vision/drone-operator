defmodule DroneAction do
  @moduledoc false

  use GenServer
  require Logger

  def start_link(_opts \\ []) do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def action(action_to_perform) do
    GenServer.cast(__MODULE__, {:action, action_to_perform})
  end

  @impl GenServer
  def init(_args) do
    {:ok, []}
  end

  @impl GenServer
  def handle_cast({:action, :shutdown_button}, state) do
    Logger.debug("SHUTTING DOWN")
    System.cmd("systemctl" ,["poweroff"])
    {:noreply, state}
  end

  def handle_cast({:action, :gear_up_button}, state) do
    Logger.debug("Gear UP")
    MAVLink.Router.pack_and_send(
        %Common.Message.CommandLong{
          command: :mav_cmd_do_set_actuator,
          confirmation: 1,
          target_component: 1,
          target_system: 1,
          param1: 1.0,
          param7: 0.0,
          param2: :nan,
          param3: :nan,
          param4: :nan,
          param5: :nan,
          param6: :nan
        }
      )
    {:noreply, state}
  end

  def handle_cast({:action, :gear_down_button}, state) do
    Logger.debug("Gear DOWN")
    MAVLink.Router.pack_and_send(
        %Common.Message.CommandLong{
          command: :mav_cmd_do_set_actuator,
          confirmation: 1,
          target_component: 1,
          target_system: 1,
          param1: -1.0,
          param7: 0.0,
          param2: :nan,
          param3: :nan,
          param4: :nan,
          param5: :nan,
          param6: :nan
        }
      )
    {:noreply, state}
  end

  def handle_cast({:action, other_action}, state) do
    Logger.warn("Unhandled action: #{Atom.to_string(other_action)}")
    {:noreply, state}
  end
end
