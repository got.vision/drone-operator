import Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :drone_operator, DroneOperatorWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "j8iZjmy0asdLSjl1i6ZnHoxd52pF1o8ZUDoDv9VcHW1Muc/7jlau/jDLFNrlrhZA",
  server: false

# In test we don't send emails.
config :drone_operator, DroneOperator.Mailer,
  adapter: Swoosh.Adapters.Test

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime

config :mavlink, dialect: Common
