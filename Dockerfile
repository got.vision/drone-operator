FROM elixir:1.13-alpine as build

# install build dependencies
RUN apk update && apk add --no-cache build-base git linux-headers

# prepare build dir
WORKDIR /app

# install hex + rebar
RUN mix local.hex --force && \
    mix local.rebar --force

# set build ENV
ENV MIX_ENV=prod

COPY mix.exs mix.lock ./
COPY config config
COPY lib lib
COPY priv priv
COPY assets assets
# COPY protos protos

# install mix dependencies
RUN mix deps.get --only prod

# Compile and release
RUN mix compile
RUN mix assets.deploy
RUN mix release

# prepare release image
FROM alpine:latest AS app
RUN apk add --no-cache openssl ncurses-libs
WORKDIR /app
RUN chown nobody:nobody /app
USER nobody:nobody
COPY --from=build --chown=nobody:nobody /app/_build/prod/rel/drone_operator ./
ENV HOME=/app
EXPOSE 4000
CMD ["bin/drone_operator", "start"]
