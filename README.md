# TODO
[ ] config for shutdown button
[ ] config for simulate-only
[ ] CSS

# DroneOperator

Simple software to do small tasks running inside an UAV. Currently this does these tasks:
- Read GPIO-pins for two buttons, which in turn issues two MavLink-commands to set servoes in separate positions. Purpose is to raise and lower landing gear.
- Read GPIO-pin for 1 button, which in turn tries to shut down the OS. This is to protect the files on disk during shutdown
- Light 1 LED to signal that software (and OS) is running


## Develop
Install asdf (https://asdf-vm.com/), and run

    asdf install

In addition Phoenix Framework is needed, and can be installed by running

    mix archive.install hex phx_new


## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `drone_operator` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:drone_operator, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/drone_operator](https://hexdocs.pm/drone_operator).

# Shutdown

## Prepare - Give access to shutdown
    sudo vim /var/lib/polkit-1/localauthority/50-local.d/allow_all_users_to_shutdown.pkla



[Allow all users to shutdown]
Identity=unix-user:*
Action=org.freedesktop.login1.power-off-multiple-sessions;org.freedesktop.login1.set-wall-message
ResultAny=yes

    sudo systemctl enable poweroff.target

    sudo systemctl deamon-reload


## Command for shutdown
```elixir
System.cmd("systemctl" ,["poweroff"])
```

# GPIO
## Rights
    sudo vim /etc/udev/rules.d/local.rules

ACTION=="add", KERNEL=="gpiochip*", GROUP="gpio", MODE="0660"


# Release + Systemd

## Command
    MIX_ENV=prod mix compile
    MIX_ENV=prod mix assets.deploy
    MIX_ENV=prod mix release --path ~/drone_operator_release --overwrite

## Systemd
sudo systemctl edit --force --full drone_operator.service

### Content of .service file


# MavLink
## Command to operate AUX1+AUX2
    MAVLink.Router.pack_and_send(%Common.Message.CommandLong{command: :mav_cmd_do_set_actuator, confirmation: 1, param1: 0.45, param2: 0.5, param3: :nan, param4: :nan, param5: :nan, param6: :nan, param7: 0.0, target_component: 1, target_system: 1})


### UP
    MAVLink.Router.pack_and_send(%Common.Message.CommandLong{command: :mav_cmd_do_set_actuator, confirmation: 1, param1: 1.0, param2: :nan, param3: :nan, param4: :nan, param5: :nan, param6: :nan, param7: 0.0, target_component: 1, target_system: 1})

### DOWN
    MAVLink.Router.pack_and_send(%Common.Message.CommandLong{command: :mav_cmd_do_set_actuator, confirmation: 1, param1: -1.0, param2: :nan, param3: :nan, param4: :nan, param5: :nan, param6: :nan, param7: 0.0, target_component: 1, target_system: 1})